// Задание
// Реализовать программу, показывающую циклично разные картинки.
//
//     Технические требования:
//
//     В папке banners лежит HTML код и папка с картинками.
//     При запуске программы на экране должна отображаться первая картинка.
//     Через 10 секунд вместо нее должна быть показана вторая картинка.
//     Еще через 10 секунд - третья.
//     Еще через 10 секунд - четвертая.
//     После того, как покажутся все картинки - этот цикл должен начаться заново.
//     При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
//     По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
//     Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//
//
//     Не обязательное задание продвинутой сложности:
//
//     При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось до показа следующей картинки.
//     Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.
//

let images = document.querySelectorAll('.image-to-show'),
    index = 0;

let slide = (elem) => {
    document.querySelector('.active').classList.remove('active');
    elem[index].classList.add('active');
    index++;
    if (index >= elem.length) {index = 0;}
};

let interval = setInterval('slide(images)', 10000);

let buttonStop = document.createElement('button'),
    buttonContinue = document.createElement('button');

buttonStop.innerHTML = "STOP";
buttonContinue.innerHTML = "Continue";

document.body.appendChild(buttonStop);
document.body.appendChild(buttonContinue);

buttonStop.addEventListener('click', () => {
    clearInterval(interval);
});
buttonContinue.addEventListener('click', () => {
    interval = setInterval('slide(images)', 10000);
});